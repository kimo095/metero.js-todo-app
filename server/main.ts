import { Meteor } from 'meteor/meteor';
import tasks from '/imports/api/tasks';

Meteor.startup(() => {
  if (Meteor.isServer) {
    // Wrap the logic inside a try-catch block to handle any potential errors
    try {
      // Ensure that the tasks collection is accessible only on the server side
      if (tasks.find().count() === 0) {
        // Insert a task only if the collection is empty
        tasks.insert({
          title: "First task",
          createdAt: new Date()
        });
      }
    } catch (error) {
      console.error("Error in startup method:", error);
    }
  }
});
