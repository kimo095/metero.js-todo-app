import { assert } from 'chai';
import sinon, { SinonStub } from 'sinon';
import tasks from '/imports/api/tasks';

describe('Server startup', function () {
  let insertStub: SinonStub<any[], any>; // Explicitly specify the type

  before(function () {
    // Create a stub for the tasks collection's insert method
    insertStub = sinon.stub(tasks, 'insert');
  });

  after(function () {
    // Restore the original insert method after all tests are done
    insertStub.restore();
  });

  it('should insert a task if the tasks collection is empty', function () {
    // Call the function directly to insert a task
    const taskData = {
      title: 'First task',
      createdAt: new Date()
    };
    insertTask(taskData);

    // Verify that the insert method was called with the correct arguments
    assert.isTrue(insertStub.calledOnce, 'insert method should be called once');
    const insertedTask = insertStub.firstCall.args[0]; // Get the first argument passed to the insert method
    assert.deepEqual(insertedTask, taskData, 'inserted task should match provided data');
  });

  // Add more test cases as needed
});

function insertTask(taskData:any) {
  tasks.insert(taskData);
}
