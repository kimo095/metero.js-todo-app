import React from 'react';
import { render} from '@testing-library/react';
import { expect } from 'chai';
import Task from '../Task';

describe('Task component', function () {
  const task = {
    _id: '1',
    title: 'Sample Task',
    onDelete: () => {}, // Mock onDelete function
  };

  it('renders task title and delete button', () => {
    const { getByText } = render(<Task {...task} />);
    expect(getByText('Sample Task')).to.exist;
    expect(getByText('Del')).to.exist;
  });

});
