import React from 'react';
import { shallow } from 'enzyme';
import chai from 'chai';
import sinon, { SinonStub } from 'sinon';
import { App } from '../App';
import { Meteor } from 'meteor/meteor'; 
import tasks from '../../api/tasks';



interface TaskProps {
    _id:string;
    title:string;
    createdAt:Date;
    completed:boolean;
}

describe('App component', () => {
  let findStub: SinonStub<any, any>;

  beforeEach(() => {
    // Mock tasks data
    const tasksData: TaskProps[] = [
      { _id: '1', title: 'Task 1', createdAt: new Date(), completed: false },
      { _id: '2', title: 'Task 2', createdAt: new Date(), completed: true }
    ];

    // Stub the find method of tasks collection
    findStub = sinon.stub() as SinonStub<any, any>;
    findStub.returns({
      fetch: () => tasksData
    });

    // Mock Meteor.subscribe and tasks.find
    sinon.stub(Meteor, 'subscribe').returns({
      ready: () => true,
      stop: () => {}
    });
    sinon.stub(tasks, 'find').callsFake(findStub);
  });

  afterEach(() => {
    // Restore the mocked functions after each test
    (Meteor.subscribe as SinonStub<any, any>).restore();
    (tasks.find as SinonStub<any, any>).restore();
  });

  it('renders tasks correctly', () => {
    // Render the component
    const wrapper = shallow(<App />);

    // Assert that tasks are rendered correctly
    chai.expect(wrapper.find('.list-group').children()).to.have.lengthOf(2);
    chai.expect(wrapper.find('Task')).to.have.lengthOf(2);
  });
});
