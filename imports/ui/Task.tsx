import React from 'react';

import {
  ListGroup,
  Button
} from "react-bootstrap";

interface TaskProps {
  _id: string;
  title: string;
  onDelete: (_id: string) => void;
}

const Task: React.FC<TaskProps> = ({ title, _id, onDelete}) => {
  return (
    <ListGroup.Item
      key={_id}
      className={`list-group-item d-flex justify-content-between task align-items-center`}
    >
      <span className="strike">{title}</span>
      <Button variant='danger' size='sm' onClick={() => onDelete(_id)}>Del</Button>
    </ListGroup.Item>
  );
};

export default Task;
