import React, { useState, useEffect } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { Meteor } from 'meteor/meteor'; 

import {
  Container,
  ListGroup,
  InputGroup,
  FormControl,
  Button,
  Form
} from "react-bootstrap";

interface TaskProps {
  _id: string;
  title: string;
  completed?: boolean;
  createdAt:Date;
}


// componets
import Header from './Header';
import Task from './Task';
import tasks from '../api/tasks';

export const App = () => {
  const [newTask, setNewTask] = useState("");
  const [dbTask, setDbTask] = useState<TaskProps[]>([]);


  // Load data from db
  useEffect(() => {
    const handle = Meteor.subscribe('tasks'); // Subscribe to tasks publication

    const fetchTasks = () => {
      if (handle.ready()) {
        const tasksList = tasks.find({}, { sort: { createdAt: -1 } }).fetch();
        setDbTask(tasksList);
      }
    };  

    fetchTasks(); // Fetch tasks initially

    const taskTracker = tasks.find().observeChanges({
      changed: fetchTasks, // Re-fetch tasks when they change
      added: fetchTasks,
      removed: fetchTasks
    });

    return () => {
      handle.stop(); // Unsubscribe when component unmounts
      taskTracker.stop();
    };
  }, []);

  const handleSubmit = (e:any) => {
    e.preventDefault();
    const trimmedTask = newTask.trim();
    if (!trimmedTask) {
      alert("Please enter a new task");
      return;
    }
  
    try {
      // Insert new task into the collection
      tasks.insert({ createdAt: new Date(), title: trimmedTask });
      
      // Update the tasks list in the component state with the new task included
      setDbTask([...dbTask, { _id: Math.random().toString(), title: trimmedTask, createdAt: new Date(), completed: false }]);
 
    } catch (error) {
      console.error("Error adding task:", error);
      alert("An error occurred while adding the task. Please try again.");
    }
     
     // Clear the input field
    setNewTask("");
  };

  //delete function
  const onDelete = async (_id:string) => {
    try {
      // Remove task from the database
      await Meteor.call('tasks.remove', _id);
      // Optionally, update the local state to reflect the deletion
      setDbTask(prevTasks => prevTasks.filter(task => task._id !== _id));
    } catch (error) {
      console.error('Error deleting task:', error);
      // Handle error here, such as showing a notification to the user
    }
  };

  
  return (
    <Container>
      <div className='mcontainer d-flex justify-content-flex-end flex-column'>
        <Header/>
        <div className='note-wrapper'>
          <ListGroup className='list-group'>
          {dbTask.map(({ title, _id}, key) => (
          <div key={key}>
        <Task title={title} _id={_id} onDelete={onDelete} />
              </div>
            ))}

          </ListGroup>
        </div>
        <div className='input-wrapper'>
          <Form onSubmit={handleSubmit}>
            <InputGroup className='mb-3'>
              <FormControl
                onChange={(e) => setNewTask(e.target.value)}
                placeholder='Enter a task' />
              <InputGroup.Text>
                <Button variant="primary" type='submit'>Submit</Button>
              </InputGroup.Text>
            </InputGroup>
          </Form>
        </div>
      </div>
    </Container>
  );
};
